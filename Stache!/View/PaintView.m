//
//  PaintView.m
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/18/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "PaintView.h"

@implementation PaintView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
