//
//  StacheFilterPreviewCell.h
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/17/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StacheFilterPreviewCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UIImageView *filterPreviewImageView;

@end
