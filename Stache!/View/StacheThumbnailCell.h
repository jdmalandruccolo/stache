//
//  StacheThumbnailCell.h
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/14/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StacheThumbnailCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailView;
@property (weak, nonatomic) IBOutlet UIView *transparency;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
