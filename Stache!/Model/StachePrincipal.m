//
//  StachePrincipal.m
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/14/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "StachePrincipal.h"

@implementation StachePrincipal

- (instancetype) initWithName:(NSString *)name
{
    
    self = [super init];
    
    if (self) {
        _name = name;
    }
    
    return self;
    
}

@end
