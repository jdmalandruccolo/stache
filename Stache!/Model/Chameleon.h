//
//  Chameleon.h
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/18/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//
//  Chameleon is a class responsible for image manipulation

#import <Foundation/Foundation.h>

@interface Chameleon : NSObject

//  resize and crop
- (UIImage *)centerAndCropImage:(UIImage *)originalImage withSideLength:(CGFloat)sideLength;


- (UIImage *)applySepiaFilterToImage:(UIImage *)originalImage;
- (UIImage *)applyMonochromeFilterWithColor:(CIColor *)color ToImage:(UIImage *)originalImage;
- (UIImage *)applyHueFiltertoImage:(UIImage *)originalImage;
- (UIImage *)applyExplosureFilterToImage:(UIImage *)originalImage;

@end
