//
//  StachePrincipal.h
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/14/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StachePrincipal : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *imageUrl;

- (instancetype) initWithName:(NSString *) name;

@end
