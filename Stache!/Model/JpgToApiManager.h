//
//  JpgToApiManager.h
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/15/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//
//  Shared Manager to access and return data using jpg.to

#import <Foundation/Foundation.h>

@interface JpgToApiManager : NSObject

- (void)imageForString:(NSString *)string withOptions:(NSArray *)options andCache:(NSCache*)cache forKey:(NSString*) key inCollectionView:(UICollectionView*)view;

- (void)imageForString:(NSString *)string withOptions:(NSArray *)options andCache:(NSCache*)cache forKey:(NSString*) key inCollectionView:(UICollectionView*)view centeringAndCroppingImageTo:(CGFloat)sideLength;

@end
