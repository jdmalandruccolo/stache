//
//  JpgToApiManager.m
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/15/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

static NSString * const     kUrlSafeString = @"+";

//  flag that determines whether a '+' replaces spaces (' ') in the jpg.to api
#define kReplaceSpaceWithPlus NO   


#import "JpgToApiManager.h"
#import "GTMHTTPFetcher.h"
#import "Chameleon.h"



@implementation JpgToApiManager


#pragma mark - instantiation
- (instancetype) init
{
    self = [super init];
    
    if (self) {
        // do nothing
    }
    
    return self;
}



#pragma mark - public api
- (void) imageForString:(NSString *)string withOptions:(NSArray*)options andCache:(NSCache*)cache forKey:(NSString*)key inCollectionView:(UICollectionView*)view
{
    NSString *jpgToPrefix = @"http://";
    NSString *queryPayload = [self removeDangerousCharactersAndConcatenate:string withOptions:options];
    NSString *jpgToSuffix = @".jpg.to/";
    NSString *jpgToQuery = [NSString stringWithFormat:@"%@%@%@",jpgToPrefix, queryPayload, jpgToSuffix];
    NSLog(@"jpgToQuery: %@", jpgToQuery);
    NSURL *jpgToUrl = [NSURL URLWithString:jpgToQuery];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError *error;
        NSString *html = [NSString stringWithContentsOfURL:jpgToUrl encoding:NSUTF8StringEncoding error:&error];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        NSLog(@"html: %@", html);
        NSURL *photoUrl = [self srcForImageFromJpgTo:html];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            /*  download the actual photo using GTMHTTPFetcher */
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            GTMHTTPFetcher *photoFetcher = [GTMHTTPFetcher fetcherWithRequest:[NSURLRequest requestWithURL:photoUrl]];
            [photoFetcher beginFetchWithCompletionHandler:^(NSData *photoData, NSError *error) {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                if (error != nil) {
                    NSLog(@"error getting photo: %@", error);
                }
                else {
                    NSLog(@"image retreived");
                    UIImage *photo = [UIImage imageWithData:photoData];
                    if (photo) {
                        [cache setObject:photo forKey:key];
                        [view reloadData];
                    }
                }
            }];
            
        });
    });
    
}


- (void) imageForString:(NSString *)string withOptions:(NSArray *)options andCache:(NSCache *)cache forKey:(NSString *)key inCollectionView:(UICollectionView *)view centeringAndCroppingImageTo:(CGFloat)sideLength
{
    NSString *jpgToPrefix = @"http://";
    NSString *queryPayload = [self removeDangerousCharactersAndConcatenate:string withOptions:options];
    NSString *jpgToSuffix = @".jpg.to/";
    NSString *jpgToQuery = [NSString stringWithFormat:@"%@%@%@",jpgToPrefix, queryPayload, jpgToSuffix];
    NSLog(@"jpgToQuery: %@", jpgToQuery);
    NSURL *jpgToUrl = [NSURL URLWithString:jpgToQuery];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError *error;
        NSString *html = [NSString stringWithContentsOfURL:jpgToUrl encoding:NSUTF8StringEncoding error:&error];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        NSLog(@"html: %@", html);
        NSURL *photoUrl = [self srcForImageFromJpgTo:html];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            /*  download the actual photo using GTMHTTPFetcher */
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            GTMHTTPFetcher *photoFetcher = [GTMHTTPFetcher fetcherWithRequest:[NSURLRequest requestWithURL:photoUrl]];
            [photoFetcher beginFetchWithCompletionHandler:^(NSData *photoData, NSError *error) {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                if (error != nil) {
                    NSLog(@"error getting photo: %@", error);
                }
                else {
                    NSLog(@"image retreived");
                    UIImage *photo = [UIImage imageWithData:photoData];
                    Chameleon *chameleon = [[Chameleon alloc] init];
                    UIImage *processedImage = [chameleon centerAndCropImage:photo withSideLength:sideLength];
                    if (processedImage) {
                        [cache setObject:processedImage forKey:key];
                        [view reloadData];
                    }
                }
            }];
            
        });
    });
}



#pragma mark - private helpers
- (NSCharacterSet*) dangerousCharacterSet
{
    NSMutableCharacterSet *dangerousCharacters = [[NSMutableCharacterSet alloc] init];
    [dangerousCharacters addCharactersInString:@" "];
    [dangerousCharacters addCharactersInString:@"&=\'$,/?;:@|{}#%~<>"];
    
    return dangerousCharacters;
}


//  jpg.to url must be of format 'http://word1word2.jpg.to/'
//  for instance, to fetch an image of Steve Jobs, the url must be of the form 'http://steve+jobs.jpg.to/' or 'http://stevejobs.jpg.to'
//  spaces are removed and replaced with + or '', depending on the value of #define kReplaceSpaceWithPlus
- (NSString*) removeDangerousCharactersAndConcatenate:(NSString*)string withOptions:(NSArray*)strings
{
    
    /*  THIS CODE DOES NOT WORK - CHARACTER SET IS INVALID
    NSMutableString *stringWithStringAndOptions = [[NSMutableString alloc] init];
    NSString *safeString = [string stringByTrimmingCharactersInSet:[self dangerousCharacterSet]];
    [stringWithStringAndOptions appendString:safeString];
    
    if (strings) {
        for (NSString *option in strings) {
            [stringWithStringAndOptions appendString:[option stringByTrimmingCharactersInSet:[self dangerousCharacterSet]]];
        }
    }
    
    return [NSString stringWithString:stringWithStringAndOptions];
    */
    
    
    NSMutableString *safeString = [[NSMutableString alloc] init];
    
    [safeString appendString:[self removeDangerousCharacters:string]];
    
    for (NSString *option in strings) {
        if (kReplaceSpaceWithPlus) {
            [safeString appendString:kUrlSafeString];
        }
        [safeString appendString:[self removeDangerousCharacters:option]];
    }
    
    return [NSString stringWithString:safeString];
    
}



- (NSString *)removeDangerousCharacters:(NSString*)string
{
    NSMutableString *safeString = [[NSMutableString alloc] init];
    
    for (int charIndex = 0; charIndex < string.length; charIndex++) {
        if ([string characterAtIndex:charIndex] != ' ' &&
            [string characterAtIndex:charIndex] != '&' &&
            [string characterAtIndex:charIndex] != '=' &&
            [string characterAtIndex:charIndex] != '\''
            ) {
            [safeString appendString:[NSString stringWithFormat:@"%C", [string characterAtIndex:charIndex]]];
        }
        else if (kReplaceSpaceWithPlus) {
            [safeString appendString:kUrlSafeString];
        }
        else {
            //  do not append any character
        }
    }
    
    return [NSString stringWithString:safeString];

}


//  parse the html for the image
//  if anything goes wrong, this method should fail silently and return nil
- (NSURL*)srcForImageFromJpgTo:(NSString*)html
{
    
    @try {
        NSLog(@"%@", html);
        //we only care about things after src="http..."
        NSArray *segments = [html componentsSeparatedByString:@"="];
        //and we know that there are two instances html style= html src=<we want this>
        NSString *urlBuilder = [segments objectAtIndex:2];
        NSLog(@"%@", urlBuilder);
        //remove the leading quote from src="http...."
        NSString *urlBuilder2 = [urlBuilder substringFromIndex:1];
        NSLog(@"%@", urlBuilder2);
        //we take everything before what is now the first quote, i.e. http:..."
        NSArray *segments2 = [urlBuilder2 componentsSeparatedByString:@"\""];
        NSString *urlString = [segments2 objectAtIndex:0];
        NSLog(@"%@", urlString);
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        return url;
    }
    @catch (NSException *exception) {
        return [NSURL URLWithString:@"http://placehold.it/500x500"];
    }
}





@end
