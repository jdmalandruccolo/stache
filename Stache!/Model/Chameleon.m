//
//  Chameleon.m
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/18/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import "Chameleon.h"



@implementation Chameleon




#pragma mark - public api - resize && crop
- (UIImage *)centerAndCropImage:(UIImage *)originalImage withSideLength:(CGFloat)sideLength
{
    
    //  get the size of the current image
    CGSize originalSize = [originalImage size];
    
    if (originalSize.width == sideLength && originalSize.height == sideLength) {
        //  we already have an image of the proper size, no more work is required
        return originalImage;
    }
    
    //  prepare the new size
    CGSize newSize = CGSizeMake(sideLength, sideLength);
    CGSize sz = CGSizeMake(newSize.width, newSize.height);
    double ratio;
    double delta;
    CGPoint offset;
    

    //  adjust the center and image ratio
    //  calculate the scale factor and offset
    //  method depends on whether image is portrait or landscape
    if (originalImage.size.width > originalImage.size.height) {
        
        ratio = newSize.height / originalImage.size.height;
        delta = ratio * (originalImage.size.width - originalImage.size.height);
        offset = CGPointMake(delta / 2, 0);
        
    }
    else {
        
        ratio = newSize.width / originalImage.size.width;
        delta = ratio * (originalImage.size.height - originalImage.size.width);
        offset = CGPointMake(0, delta / 2);
        
    }
    
    //  assemble the clipping rectangle
    CGRect clipSquare = CGRectMake(-offset.x, -offset.y,
                                   ratio * originalSize.width, ratio * originalSize.height);
    
    
    //  start a new context with a scale factor of 0.0 so retina displays get a high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    }
    else {
        UIGraphicsBeginImageContext(sz);
    }
    
   
    //  clip and create the new image
    UIRectClip(clipSquare);
    [originalImage drawInRect:clipSquare];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;

}



- (UIImage *)applySepiaFilterToImage:(UIImage *)originalImage
{
    CIImage *inputImage = [[CIImage alloc] initWithImage:originalImage];
    CIFilter *filter = [CIFilter filterWithName:@"CISepiaTone"
                                  keysAndValues: kCIInputImageKey, inputImage,
                        @"inputIntensity", @0.8, nil];
    CIImage *outputImage = [filter outputImage];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgimg = [context createCGImage:outputImage fromRect:[outputImage extent]];
    UIImage *newImage = [UIImage imageWithCGImage:cgimg];
    
    CGImageRelease(cgimg);
    
    return newImage;
}

- (UIImage *)applyMonochromeFilterWithColor:(CIColor *)color ToImage:(UIImage *)originalImage
{
    CIImage *inputImage = [[CIImage alloc] initWithImage:originalImage];
    CIFilter *filter = [CIFilter filterWithName:@"CIColorMonochrome"
                                  keysAndValues: kCIInputImageKey, inputImage,
                        @"inputIntensity", @0.8,
                        @"inputColor", color,
                        nil];
    CIImage *outputImage = [filter outputImage];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgimg = [context createCGImage:outputImage fromRect:[outputImage extent]];
    UIImage *newImage = [UIImage imageWithCGImage:cgimg];
    
    CGImageRelease(cgimg);
    
    return newImage;
}

- (UIImage *)applyHueFiltertoImage:(UIImage *)originalImage
{
    CIImage *inputImage = [[CIImage alloc] initWithImage:originalImage];
    CIFilter *filer = [CIFilter filterWithName:@"CIHueAdjust"];
    [filer setDefaults];
    [filer setValue:inputImage forKey:@"inputImage"];
    [filer setValue:[NSNumber numberWithFloat:3.0f] forKey:@"inputAngle"];
    CIImage *outputImage = [filer outputImage];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgimg = [context createCGImage:outputImage fromRect:[outputImage extent]];
    UIImage *newImage = [UIImage imageWithCGImage:cgimg];
    
    CGImageRelease(cgimg);
    
    return newImage;
    
}

- (UIImage *)applyExplosureFilterToImage:(UIImage *)originalImage
{
    CIImage *inputImage = [[CIImage alloc] initWithImage:originalImage];
    CIFilter *filer = [CIFilter filterWithName:@"CIExposureAdjust"];
    [filer setDefaults];
    [filer setValue:inputImage forKey:@"inputImage"];
    [filer setValue:[NSNumber numberWithFloat:2.0f] forKey:@"inputEV"];
    CIImage *outputImage = [filer outputImage];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgimg = [context createCGImage:outputImage fromRect:[outputImage extent]];
    UIImage *newImage = [UIImage imageWithCGImage:cgimg];
    
    CGImageRelease(cgimg);
    
    return newImage;
}

@end
