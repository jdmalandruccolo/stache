//
//  StachePrincipalViewController.m
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/16/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//
//  Abstract superclass containing a collection view of preview cell


CGFloat const    kThumbnailImageSize = 175.0;


#import "StachePrincipalViewController.h"
#import "StacheThumbnailCell.h"
#import "StachePrincipal.h"
#import "JpgToApiManager.h"
#import "StacheEditPhotoViewController.h"
#import "StacheMovieStarViewController.h"
#import "StachePoliticianViewController.h"

@interface StachePrincipalViewController ()

@property (strong, nonatomic) NSCache *imageCache;
@property (strong, nonatomic) UICollectionView *collectionView;


@end

@implementation StachePrincipalViewController


#pragma mark - life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _imageCache = [[NSCache alloc] init];
}


- (void)setCollectionView:(UICollectionView *)collectionView
{
    _collectionView = collectionView;
}

#pragma mark - segue
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:kMovieStarSegueIdentifier] || [[segue identifier] isEqualToString:kPoliticianSegue]) {
        
        StacheEditPhotoViewController *destinationVC = [segue destinationViewController];
        StacheThumbnailCell *sendingCell = (StacheThumbnailCell*)sender;
        
        [destinationVC setRepresentedImage:sendingCell.thumbnailView.image];
    }
}


#pragma mark - unwind segues
- (IBAction) didFinishEditingPhoto: (UIStoryboardSegue *)segue
{
    //  do nothing
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[self principals] count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"thumbnail cell";
    StacheThumbnailCell *cell = (StacheThumbnailCell*)[self.collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    StachePrincipal *principal = [self.principals objectAtIndex:indexPath.item];
    
    //  assign the cell's properties
    cell.name.text = principal.name;
    cell.thumbnailView.image = ![self.imageCache objectForKey:principal.name] ? [UIImage imageNamed:@"placeholder_avatar"] : [self.imageCache objectForKey:principal.name];
    
    //  if the cache is empty, get the image the expensive way
    if (![self.imageCache objectForKey:principal.name]) {
        JpgToApiManager *apiManager = [[JpgToApiManager alloc] init];
        [apiManager imageForString:principal.name
                       withOptions:nil
                          andCache:self.imageCache forKey:principal.name
                  inCollectionView:collectionView
       centeringAndCroppingImageTo:kThumbnailImageSize];
    }
    
    
    return cell;
    
}


@end
