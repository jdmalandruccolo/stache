//
//  StacheEditPhotoViewController.m
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/17/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//


CGFloat const   kPrimaryImageViewSideLength = 291.0;
CGFloat const   kFilterPreviewImageViewSideLength = 120.0;

#import "StacheEditPhotoViewController.h"
#import "StacheFilterPreviewCell.h"
#import <QuartzCore/QuartzCore.h>

@interface StacheEditPhotoViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *primaryImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) PaintView *paintView;
@property (weak, nonatomic) IBOutlet UICollectionView *filterCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *drawingEnabledIndicatorButton;
@property (strong, nonatomic) UIImage *originalImageCopy;
@property (nonatomic) BOOL drawingEnabled;
@property BOOL shouldMerge;


- (IBAction)exportCurrentImage:(UIBarButtonItem *)sender;
- (IBAction)saveCurrentImage:(UIBarButtonItem *)sender;
- (IBAction)toggleDrawingEnabled:(UIButton *)sender;

@end

@implementation StacheEditPhotoViewController

#pragma mark - life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	Chameleon *chameleonInstance = [[Chameleon alloc] init];
    self.primaryImageView.image = [chameleonInstance centerAndCropImage:self.representedImage
                                                         withSideLength:kPrimaryImageViewSideLength];
    self.originalImageCopy = [self.primaryImageView.image copy];
    self.drawingEnabled = NO;
    self.shouldMerge = YES;
}


- (void)viewDidAppear:(BOOL)animated
{
    //  OK GLASS - make a paint view
    self.paintView = [[PaintView alloc] initWithFrame:self.backgroundImageView.bounds];
    self.paintView.lineColor = [UIColor blackColor];
    self.paintView.delegate = self;
    [self.backgroundImageView addSubview:self.paintView];
}


#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
#warning unimplmented method - TODO: refactor the filter application code
    return 5;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"filter preview cell";
    StacheFilterPreviewCell * cell = [self.filterCollectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    Chameleon *chameleonInstance = [[Chameleon alloc] init];
    
    UIImage *adjustedImage = [chameleonInstance centerAndCropImage:self.representedImage
                                                               withSideLength:kFilterPreviewImageViewSideLength];
    
    
    switch (indexPath.item) {
        case 0:
            cell.filterPreviewImageView.image = adjustedImage;
            break;
            
        case 1:
            cell.filterPreviewImageView.image = [chameleonInstance applySepiaFilterToImage:adjustedImage];
            break;
        case 2:
            cell.filterPreviewImageView.image = [chameleonInstance applyHueFiltertoImage:adjustedImage];
            break;
        case 3:
            cell.filterPreviewImageView.image = [chameleonInstance applyExplosureFilterToImage:adjustedImage];
            break;
        case 4:
            cell.filterPreviewImageView.image = [chameleonInstance applyMonochromeFilterWithColor:[CIColor colorWithRed:1.0f green:0.0f blue:1.0f alpha:1.0f] ToImage:adjustedImage];
            break;
        default:
            assert(1 == 0);
            break;
    }
    
    
    
    return cell;
    
}


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    Chameleon *chameleonInstance = [[Chameleon alloc] init];
    
    switch (indexPath.item) {
        case 0:
            self.primaryImageView.image = self.originalImageCopy;
            break;
            
        case 1:
            self.primaryImageView.image = [chameleonInstance applySepiaFilterToImage:[self.originalImageCopy copy]];
            break;
        case 2:
            self.primaryImageView.image = [chameleonInstance applyHueFiltertoImage:[self.originalImageCopy copy]];
            break;
        case 3:
            self.primaryImageView.image = [chameleonInstance applyExplosureFilterToImage:[self.originalImageCopy copy]];
            break;
        case 4:
            self.primaryImageView.image = [chameleonInstance applyMonochromeFilterWithColor:[CIColor colorWithRed:1.0f green:0.0f blue:1.0f alpha:1.0f] ToImage:[self.originalImageCopy copy]];
            break;
        default:
            assert(1 == 0);
            break;
    }
}





#pragma mark - actions
- (IBAction)exportCurrentImage:(UIBarButtonItem *)sender
{
    //  post to App Engine
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://flickr.com"]];
    
    //  TODO: use the api to post
    
}

- (IBAction)saveCurrentImage:(UIBarButtonItem *)sender
{
    UIImage *savedImage = [self mergeTwoImages:self.primaryImageView.image :self.backgroundImageView.image];
    
    NSString *imgName= @"imgname.png";
    [[NSUserDefaults standardUserDefaults]setValue:imgName forKey:@"imageName"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:imgName];
    NSData *imageData = UIImagePNGRepresentation(savedImage);
    [imageData writeToFile:savedImagePath atomically:NO];
}

- (IBAction)toggleDrawingEnabled:(UIButton *)sender
{
    self.drawingEnabled = !self.drawingEnabled;
    
    if (self.drawingEnabled == YES) {
        self.drawingEnabledIndicatorButton.backgroundColor = [UIColor greenColor];
        self.backgroundImageView.userInteractionEnabled = YES;
        
    }
    else {
        self.drawingEnabledIndicatorButton.backgroundColor = [UIColor lightGrayColor];
        self.backgroundImageView.userInteractionEnabled = NO;
    }
    
}


#pragma mark - PaintView delegate
- (void)paintView:(PaintView *)paintView finishedTrackingPath:(CGPathRef)path inRect:(CGRect)painted
{
    NSLog(@"finishTrackingPath");
    if (self.shouldMerge) {
        NSLog(@"calling mergePaintToBackgroundView");
        [self mergePaintToBackgroundView:painted];
    }

}

/*******************************************************************************
 * @method          mergePaintToBackgroundView
 * @abstract        Combine the last painted image into the current background image
 * @description
 *******************************************************************************/
- (void)mergePaintToBackgroundView:(CGRect)painted
{
    // Create a new offscreen buffer that will be the UIImageView's image
    CGRect bounds = self.backgroundImageView.bounds;
    UIGraphicsBeginImageContextWithOptions(bounds.size, NO, self.backgroundImageView.contentScaleFactor);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Copy the previous background into that buffer.  Calling CALayer's renderInContext: will redraw the view if necessary
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    [self.backgroundImageView.layer renderInContext:context];
    
    // Now copy the painted contect from the paint view into our background image
    // and clear the paint view.  as an optimization we set the clip area so that we only copy the area of paint view
    // that was actually painted
    CGContextClipToRect(context, painted);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    [self.paintView.layer renderInContext:context];
    //[self.paintView erase];
    
    // Create UIImage from the context
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    self.backgroundImageView.image = image;
    UIGraphicsEndImageContext();
    
    
}

-(UIImage *) mergeTwoImages: (UIImage *)bottomImage : (UIImage *)upperImage

{
    
    UIImage *image = upperImage;
    
    CGSize newSize = CGSizeMake(bottomImage.size.width, bottomImage.size.height);
    
    UIGraphicsBeginImageContext( newSize );
    
    // Use existing opacity as is
    
    [bottomImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Apply supplied opacity
    
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height) blendMode:kCGBlendModeNormal alpha:1.0];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
    
}

@end
