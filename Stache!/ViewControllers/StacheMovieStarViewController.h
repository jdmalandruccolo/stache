//
//  StacheMovieStarViewController.h
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/14/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

extern NSString * const kMovieStarSegueIdentifier;

#import <UIKit/UIKit.h>
#import "StachePrincipalViewController.h"


@interface StacheMovieStarViewController : StachePrincipalViewController <UICollectionViewDelegateFlowLayout>


@end
