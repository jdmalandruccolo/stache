//
//  StachePrincipalViewController.h
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/16/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StachePrincipalViewController : UIViewController <UICollectionViewDataSource>

@property (strong, nonatomic) NSArray *principals;


- (void)setCollectionView:(UICollectionView *)collectionView;

@end
