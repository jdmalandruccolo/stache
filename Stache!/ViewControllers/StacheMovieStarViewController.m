//
//  StacheMovieStarViewController.m
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/14/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//


//  segue identifiers
NSString * const     kMovieStarSegueIdentifier = @"movie star segue";



#import "StacheMovieStarViewController.h"
#import "StacheEditPhotoViewController.h"
#import "StacheThumbnailCell.h"




@interface StacheMovieStarViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;


@end

@implementation StacheMovieStarViewController


#pragma mark - life cycle
- (void)viewDidLoad
{
    NSLog(@"StacheMovieStarViewController viewDidLoad");
    [super viewDidLoad];
    [super setCollectionView:self.collectionView];
}








@end
