//
//  StacheEditPhotoViewController.h
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/17/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//
//  Class to edit a photo

#import <UIKit/UIKit.h>
#import "Chameleon.h"
#import "PaintView.h"

@interface StacheEditPhotoViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PaintViewDelegate>


@property (weak, nonatomic) UIImage *representedImage;


- (void)paintView:(PaintView*)paintView finishedTrackingPath:(CGPathRef)path inRect:(CGRect)painted;

@end
