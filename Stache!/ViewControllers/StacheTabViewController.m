//
//  StacheTabViewController.m
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/14/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//


static NSString const *     kMovieStarKey = @"movie stars";
static NSString const *     kPoliticianKey = @"politicians";

#import "StacheTabViewController.h"
#import "StachePrincipal.h"
#import "StacheMovieStarViewController.h"
#import "StachePoliticianViewController.h"

@interface StacheTabViewController ()

@property (strong, nonatomic) NSMutableDictionary *category;

@end

@implementation StacheTabViewController


#pragma mark - life cycle
- (void)viewDidLoad
{
    
    NSLog(@"StacheTabViewController viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.category = [[NSMutableDictionary alloc] init];
    [self.category setObject:[self arrayForMovieStarsWithShuffle:NO] forKey:kMovieStarKey];
    [self.category setObject:[self arrayForPoliticiansWithShuffle:NO] forKey:kPoliticianKey];
    
    NSArray *viewControllers = [self viewControllers];
    
    //  prepare StacheMovieStarViewController
    StacheMovieStarViewController *movieStarVC = [viewControllers objectAtIndex:0];
    [movieStarVC setPrincipals:[self.category objectForKey:kMovieStarKey]];
    
    NSLog(@"finished preparing movie star vc");
    
    //  prepare StachePoliticianViewController
    StachePoliticianViewController *politicianVC = [viewControllers objectAtIndex:1];
    [politicianVC setPrincipals:[self.category objectForKey:kPoliticianKey]];
}


#pragma mark - model builder helpers
- (NSArray *) arrayForMovieStarsWithShuffle: (BOOL) shuffle
{
    //  load names
    NSMutableArray *movieStars = [[NSMutableArray alloc] init];
    [movieStars addObjectsFromArray:[self arrayFromPlist:@"actresses"]];
    [movieStars addObjectsFromArray:[self arrayFromPlist:@"actors"]];
    
    
    if (shuffle) {
        //  TODO: shuffle the array
    }
    
    return [self createPrincipalsFromArray:movieStars];
    
}


- (NSArray *) arrayForPoliticiansWithShuffle: (BOOL) shuffle
{
    //  load names
    NSMutableArray *politicians = [[NSMutableArray alloc] init];
    [politicians addObjectsFromArray:[self arrayFromPlist:@"senators"]];
    [politicians addObjectsFromArray:[self arrayFromPlist:@"presidents"]];
    
    if (shuffle) {
        //  TODO: shuffle the array
    }
    
    return [self createPrincipalsFromArray:politicians];
}


- (NSArray *) createPrincipalsFromArray: (NSArray*) array
{
    
    NSMutableArray *principals = [[NSMutableArray alloc] init];
    for (NSString *name in array) {
        StachePrincipal *principal = [[StachePrincipal alloc] initWithName:name];
        [principals addObject:principal];
    }
    
    return [NSArray arrayWithArray:principals];
    
}


- (NSArray *) arrayFromPlist:(NSString*) plist
{
    NSBundle *bundle = [NSBundle mainBundle];
    
    //  read plist
    NSURL *url = [bundle URLForResource:plist withExtension:@"plist"];
    assert(url);
    NSArray *array = [NSArray arrayWithContentsOfURL:url];
    assert(array);
    
    return array;
    
}


@end
