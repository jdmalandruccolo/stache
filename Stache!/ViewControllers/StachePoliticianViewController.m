//
//  StachePoliticianViewController.m
//  Stache!
//
//  Created by Joseph Malandruccolo on 5/14/13.
//  Copyright (c) 2013 Joseph Malandruccolo. All rights reserved.
//


NSString * const    kPoliticianSegue = @"politician segue";



#import "StachePoliticianViewController.h"

@interface StachePoliticianViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation StachePoliticianViewController


#pragma mark - life cycle
- (void)viewDidLoad
{
    NSLog(@"StachePoliticianViewController viewDidLoad");
    [super viewDidLoad];
	[super setCollectionView:self.collectionView];
}



@end
