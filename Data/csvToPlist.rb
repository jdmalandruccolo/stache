require 'csv'

file_name = ARGV[0]
puts "Reading from file #{file_name}" # = > file name to read

plist_header = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">'
array_start = '<array>'
array_end = '</array>'
plist_end = '</plist>'

names = []
# read the csv file
CSV.foreach(file_name, headers: true) do |row|
	names.push(row.to_hash["Name"]) unless row.length <= 0
end

#write to plist
f = File.new("file_name.plist", "w+")
f.write(plist_header)
f.write(array_start)

names.each do |name|
	f.write("<string>#{name}</string>")
end

f.write(array_end)
f.write(plist_end)
f.close